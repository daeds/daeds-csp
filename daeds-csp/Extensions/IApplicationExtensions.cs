using System;
using Microsoft.AspNetCore.Builder;

namespace Daeds.Csp
{
    public static class IApplicationBuilderExtensions
    {
        /// <summary>
        /// Adds a Content Security Policy header
        /// to the response.
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/></param>
        /// <param name="optionsDelegate">Delegate for configuring the header</param>
        /// <returns>The <see cref="IApplicationBuilder"/></returns>
        public static IApplicationBuilder UseCsp(this IApplicationBuilder app, Action<CspOptions> optionsDelegate)
        {
            var options = new CspOptions();

            if (optionsDelegate != null)
            {
                optionsDelegate(options);
            }
            
            return app.UseMiddleware<CspMiddleware>(options);
        }
    }
}