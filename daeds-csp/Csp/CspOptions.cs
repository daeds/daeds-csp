using System;
using System.Collections.Generic;

namespace Daeds.Csp
{
    public class CspOptions
    {
        public string Default { get; set; }
        public string Script { get; set; }
        public string Img { get; set; }
        public string Style { get; set; }
        public string Connect { get; set; }
        public string Font { get; set; }
        public string Object { get; set; }
        public string Media { get; set; }
        public string Child { get; set; }
        public string Sandbox { get; set; }
        public string FormAction { get; set; }
        public string FrameAncestors { get; set; }
        public string PluginTypes { get; set; }
        public bool ReportOnly { get; set; }
        public string ReportUri { get; set; }
        public string Manifest { get; set; }

        internal List<(string directive, string value)> CustomDirectives { get; set; } = new List<(string directive, string value)>();
        public void AddCustomDirective(string directive, string value)
        {
            if (string.IsNullOrWhiteSpace(directive)) return;

            CustomDirectives.Add((directive, value ?? string.Empty));
        }
    }
}