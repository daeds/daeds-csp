using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Daeds.Csp
{
    public class CspMiddleware
    {
        private readonly RequestDelegate _next = null;
        
        private readonly (string Header, string Value) _csp;

        public CspMiddleware(RequestDelegate next, CspOptions options)
        {
            var cspBuilder = new CspBuilder(options);

            _csp = cspBuilder.Build();
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            httpContext.Response.Headers.Add(_csp.Header, _csp.Value);

            await _next.Invoke(httpContext);
        }
    }
}