using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daeds.Csp
{
    public class CspBuilder
    {
        private readonly CspOptions _options = null;
        
        public CspBuilder(CspOptions options)
        {
            _options = options;
        }

        public (string Header, string Value) Build()
        {
            return (GetHeader(), GetCsp());
        }

        private string GetHeader()
        {
            return _options.ReportOnly
                ? "Content-Security-Policy-Report-Only"
                : "Content-Security-Policy";
        }

        private string GetCsp()
        {
            var result = new StringBuilder();

            result.Append(GetDirective("default-src", _options.Default));
            result.Append(GetDirective("script-src", _options.Script));
            result.Append(GetDirective("style-src", _options.Style));
            result.Append(GetDirective("img-src", _options.Img));
            result.Append(GetDirective("connect-src", _options.Connect));
            result.Append(GetDirective("font-src", _options.Font));
            result.Append(GetDirective("object-src", _options.Object));
            result.Append(GetDirective("media-src", _options.Media));
            result.Append(GetDirective("child-src", _options.Child));
            result.Append(GetDirective("manifest-src", _options.Manifest));
            result.Append(GetCustomDirectives(_options.CustomDirectives));
            result.Append(GetDirective("sandbox", _options.Sandbox));
            result.Append(GetDirective("form-action", _options.FormAction));
            result.Append(GetDirective("frame-ancestors", _options.FrameAncestors));
            result.Append(GetDirective("plugin-types", _options.PluginTypes));
            result.Append(GetDirective("report-uri", _options.ReportUri));

            return result.ToString().Trim();
        }

        private string GetCustomDirectives(List<(string directive, string value)> list)
        {
            return list
                .Select(x => GetDirective(x.directive, x.value))
                .Aggregate(string.Empty, (result, next) => result + next);
        }

        private string GetDirective(string name, string value)
        {
            return string.IsNullOrWhiteSpace(value)
                ? string.Empty
                : $"{name} {value.TrimEnd(';')}; ";
        }
    }
}