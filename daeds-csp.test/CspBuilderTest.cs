using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace Daeds.Csp.Test
{
    public class CspBuilderTest
    {
        private CspOptions _options = null;

        private (string Header, string Value) Csp => (new CspBuilder(_options)).Build();

        public CspBuilderTest()
        {
            _options = new CspOptions();
        }

        [Fact]
        public void DefaultHttpHeaderIsContentSecurityPolicy()
        {
            Assert.Equal("Content-Security-Policy", Csp.Header);
        }

        [Fact]
        public void HttpHeaderIsContentSecurityPolicyReportOnly()
        {
            _options.ReportOnly = true;
            Assert.Equal("Content-Security-Policy-Report-Only", Csp.Header);
        }

        [Fact]
        public void EmptyValueIfEmptyCspOptions()
        {
            Assert.Equal(string.Empty, Csp.Value);
        }

        [Fact]
        public void DefaultAdded()
        {
            _options.Default = "'self' *.google.se";
            Assert.Equal("default-src 'self' *.google.se;", Csp.Value);
        }

        [Fact]
        public void DefaultAddedWithSemicolon()
        {
            _options.Default = "*.somedomain.com;";
            Assert.Equal("default-src *.somedomain.com;", Csp.Value);
        }

        [Fact]
        public void ScriptAdded()
        {
            _options.Script = "'self'";
            Assert.Equal("script-src 'self';", Csp.Value);
        }

        [Fact]
        public void StyleAdded()
        {
            _options.Style = "'self'";
            Assert.Equal("style-src 'self';", Csp.Value);
        }

        [Fact]
        public void ImgAdded()
        {
            _options.Img = "'self'";
            Assert.Equal("img-src 'self';", Csp.Value);
        }

        [Fact]
        public void ConnectAdded()
        {
            _options.Connect = "'self'";
            Assert.Equal("connect-src 'self';", Csp.Value);
        }

        [Fact]
        public void FontAdded()
        {
            _options.Font = "'self'";
            Assert.Equal("font-src 'self';", Csp.Value);
        }

        [Fact]
        public void ObjectAdded()
        {
            _options.Object = "'self'";
            Assert.Equal("object-src 'self';", Csp.Value);
        }

        [Fact]
        public void MediaAdded()
        {
            _options.Media = "'self'";
            Assert.Equal("media-src 'self';", Csp.Value);
        }

        [Fact]
        public void ChildAdded()
        {
            _options.Child = "'self'";
            Assert.Equal("child-src 'self';", Csp.Value);
        }

        [Fact]
        public void ManifestAdded()
        {
            _options.Manifest = "'self'";
            Assert.Equal("manifest-src 'self';", Csp.Value);
        }

        [Fact]
        public void SandboxAdded()
        {
            _options.Sandbox = "allow-forms";
            Assert.Equal("sandbox allow-forms;", Csp.Value);
        }

        [Fact]
        public void FormActionAdded()
        {
            _options.FormAction = "'self'";
            Assert.Equal("form-action 'self';", Csp.Value);
        }

        [Fact]
        public void FrameAncestorsAdded()
        {
            _options.FrameAncestors = "'self'";
            Assert.Equal("frame-ancestors 'self';", Csp.Value);
        }

        [Fact]
        public void PluginTypesAdded()
        {
            _options.PluginTypes = "application/pdf";
            Assert.Equal("plugin-types application/pdf;", Csp.Value);
        }

        [Fact]
        public void ReportUriAdded()
        {
            _options.ReportUri = "/some-uri";
            Assert.Equal("report-uri /some-uri;", Csp.Value);
        }

        [Fact]
        public void OneCustomDirectiveAdded()
        {
            _options.AddCustomDirective("my-custom-directive", "some-value");
            Assert.Equal("my-custom-directive some-value;", Csp.Value);
        }

        [Fact]
        public void SeveralCustomDirectivesAdded()
        {
            _options.AddCustomDirective("directive1", "value1");
            _options.AddCustomDirective("directive2", "value2");
            Assert.Equal("directive1 value1; directive2 value2;", Csp.Value);
        }

        [Fact]
        public void SeveralDirectivesAdded()
        {
            _options.Default = "'self'";
            _options.Img = "*.img.com;";
            _options.Font = "*.fonts.google.com";
            Assert.Equal("default-src 'self'; img-src *.img.com; font-src *.fonts.google.com;", Csp.Value);
        }
    }
}
