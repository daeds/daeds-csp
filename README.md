# Daeds.Csp

A simple middleware for ASP.NET Core 2+ to add the Content-Security-Policy http header.

## Usage

In Startup.cs, add the following using-statement:

```csharp
using Daeds.Csp;
```

Then, in the Configure-method, you add the middleware and configure the header's directives:

```csharp
app.UseCsp(options =>
{
    // configure the CSP directives, i.e:
    options.Default = "'self'";
});
```

Be sure to add the middleware before the app.UseMvc() call.